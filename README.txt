Evidencia faktur
---------------------------------------------------------------------------
Vytvorte aplik�ciu pre evidenciu fakt�r. Aplik�cia bude uchov�va? zoznam fakt�r, zoznam z�kazn�kov a zoznam tovarov.
O z�kazn�koch bude uchov�va? inform�cie:
- meno (stae� reprezentova? ako jeden String)
- adresa (stae� reprezentova? ako jeden String)
O tovaroch bude uchov�va? inform�cie:
- n�zov a struen� popis tovaru (stae� reprezentova? ako jeden String)
- cena
Fakt�ra bude obsahova? inform�cie:
- d�tum vystavenia (stae� reprezentova? ako String)
- z�kazn�k
- zoznam fakturovan�ch tovarov a mno�stvo ka�d�ho druhu tovaru
- pri v�pise fakt�ry sa bude zobrazova? aj celkov� suma fakturovan�ch tovarov
Program bude umo�oova?:
- vytv�ranie nov�ch z�kazn�kov
- vytv�ranie nov�ch tovarov
- vytv�ranie nov�ch fakt�r
- editova? inform�cie o existuj�cich z�kazn�koch
- editova? inform�cie o exituj�cich tovaroch
Po edit�cii inform�cii o existuj�cich z�kazn�koch, alebo tovaroch sa obsah existuj�cich fakt�r nebude meni?